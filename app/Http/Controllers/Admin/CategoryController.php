<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends AdminController
{
    public function index()
    {
        $categories = Category::paginate(10);

        return view('admin.category.index')->with([
            'categories'=> $categories
        ]);
    }


    /**
     * trang thêm mới chuyên mục
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        return view('admin.category.form');
    }

    /**
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $data = $request->all();
//       dd($data);
//Validate
        $this->validate($request,[
            'title'=>'required|max:10',
        ]);
//Save Database
        $category = Category::create($data);

        if($category) {
            return redirect()->route('admin.category');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    /**
     * hiển thị trang để chỉnh sửa thông tin của một Category cụ thể.
     */
    public function EDI(Request $request)
    {
        $id = $request->id;
        $category = Category::findOrFail($id);
//        dd($category);
        return view('admin/category/formEdit')->with('category',$category);

    }

    /**
     * Update the specified resource in storage.
     */
    /**
     *cập nhật thông tin của một Category cụ thể trong cơ sở dữ liệu
     */

    public function updateCate(Request $request, int $id)
    {
        // Tìm danh mục được cập nhật
        $category = Category::findOrFail($id);

        // Validate the input
        $request->validate([
            'name' => 'required|max:50',
            'desc' => 'required',
        ]);

        // Cập nhật category
        $category->name = $request->input('name');
        $category->desc = $request->input('desc');
        $category->save();

        // Kiểm tra xem cập nhật dữ liệu đã thành công hay chưa
        if ($category->wasChanged()) {
            // Chuyển hướng trang trở về khi thành công
            return redirect()->route('admin.category')->with('success', 'Category updated successfully');
        } else {
            // Nếu không có sự thay đổi, hiển thị thông báo lỗi
            return redirect()->back()->with('error', 'Failed to update category');
        }

    }


    /**
     * Remove the specified resource from storage.
     */
    /**
     * xóa một Category cụ thể khỏi cơ sở dữ liệu
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('admin.category')->with('success', 'Category has been deleted');
    }

    //Cách 2

    public function DEL(Request $request)
    {
        $id = $request->id;
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('admin.category');
    }

}
