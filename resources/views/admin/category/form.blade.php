@extends('admin.layout')
@section('content')
    <h2>{{__('Add category')}}</h2>
{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            <div class="alert alert-danger">--}}
{{--                <ul>--}}
{{--                    @foreach ($errors->all() as $error)--}}
{{--                        <li>{{ $error }}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

    {!! Form::open(['name' => 'Admin.category.store']) !!}
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">{{__('Name')}}</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        @error('name')
        <div id="emailHelp" class="form-text text-danger">Bắt buộc nhập tiêu đề.</div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">{{__('Description')}}</label>
        <input type="text" name="desc" class="form-control" id="exampleInputPassword1">
    </div>
    <div class="mb-3 form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    {!! Form::close() !!}
@endsection
@section('page_script')
    <script>

    </script>
@endsection
