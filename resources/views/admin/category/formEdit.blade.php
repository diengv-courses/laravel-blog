
@extends('admin.layout')
@section('content')
    <h2>Edit Categories</h2>
    {!! Form::open(['route' => ['admin.edit.updateCate', $category->id], 'method' => 'post']) !!}
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">{{__('Name')}}</label>
        <input type="text" name="name" class="form-control" value="{{$category->name}}" aria-describedby="emailHelp">
        @error('name')
        <div id="emailHelp" class="form-text text-danger">Bắt buộc nhập tiêu đề.</div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">{{__('Description')}}</label>
        <input type="text" name="desc" class="form-control" value="{{$category->desc}}">
    </div>
    <div class="mb-3 form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
    {!! Form::close() !!}
@endsection
@section('page_script')
    <script>

    </script>
@endsection
