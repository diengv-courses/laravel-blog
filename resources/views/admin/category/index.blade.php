{{--Ke thua layout--}}
@extends('admin.layout')
@section('content')
    <h2 class="title">{{__('List Category')}}</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Title</th>
            <th scope="col">Desc</th>
            <th scope="col">Actions</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <th scope="row">{{$category->id}}</th>
                <td>{{$category->name}}</td>
                <td>{{$category->desc}}</td>
                <td><a href="{{route('admin.edit')}}?id={{$category->id}}"><button type="button" class="btn btn-primary" >Edit</button></a></td>
                <td><a href="{{route('admin.category.destroy')}}?id={{$category->id}}"><button type="button" class="btn btn-danger" >Delete</button></a></td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <div>
        <a href="{{route('admin.category.create')}}" class="btn btn-primary"> {{__('Add New')}} </a>
    </div>

    {{ $categories->links('vendor.pagination.bootstrap-5') }}

@endsection
